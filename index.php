<!DOCTYPE html>
<html>
    <head>
        <title>Login Dinâmico Ajax</title>
        <script type="text/javascript" src="js/jquery-2.2.1.min.js"></script>
        <script type="text/javascript" src="js/functions.js"></script>
        <style>
            .blue{color:#0066CC}
            .red{color:#FF0000}
        </style>
    </head>
    <body>
        <form action="includes/controller/UsuarioController.php" method="post" name="frm_login">
            <h3>Efetuar login:</h3>
            <input type="text" name="login" placeholder="Login" autofocus required/>
            <input type="password" name="senha" placeholder="Senha" required/>
            <button type="submit" name="btn_login">Entrar</button>
        </form>
        <div id="resultado"></div>
    </body>
</html>
