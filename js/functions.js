$(document).ready(function () {
    $("button[name=btn_login]").click(function (e) {
        $login = $("input[name=login]");
        $senha = $("input[name=senha]");
        if ($login.val() === "") {
            $login.focus();
            return; //retorna nulo
        }
        else if ($senha.val() === "") {
            $senha.focus();
            return;
        }
        //PASSOU! GO AJAX!
        else {
            $("#resultado").html("Autenticando...");
            e.preventDefault();
            /**Função ajax nativa da jQuery, onde passamos como par�metro o endere�o do arquivo que queremos chamar, os dados que ir� receber, e criamos de forma encadeada a fun��o que ir� armazenar o que foi retornado pelo servidor, para poder se trabalhar com o mesmo */
            $.post("includes/controller/UsuarioController.php?acao=autenticar", {login: $login.val(), senha: $senha.val()},
            function (retorno) {
                $("#resultado").html(retorno);
            } //function(retorno)
            ); //$.post()
        }
    });
});